is_mobile = () ->
	return $(window).width()<769

scroll_to_section = (id) ->
	if id.substring(0,1) isnt '#'
		id = '#' + id
	goal = $(id).offset().top;
	goalPx = goal
	if not is_mobile()
		goalPx = goal - 100
	# if $(window).scrollTop()<goal

	$('html,body').animate {scrollTop: goalPx}, 1000
	return

resize_header_nav = () ->
	client_height = $(window).height()
	minHeight = 0
	nav_height = 0
	ul_height = 0
	if not is_mobile()	
		client_height = if client_height<minHeight then minHeight else client_height
		nav_height = 100
		minHeight = $('.lulab-logo').height()+$('nav').height()*2
		$('nav').css
			'display': 'block'
		ul_height = nav_height
	else
		nav_height = $(window).height()
		ul_height = nav_height - 60

	$('header').css
		'line-height': client_height + 'px'
		'height': client_height

	$('nav').css
		'height': nav_height

	$('ul', 'nav').css
		'height': ul_height
	return

toggle_navbar = (force_action) ->
	action = 'open'
	pos = 0
	left = 0
	switch force_action
		when 'open' then action = 'open'
		when 'close' then action = 'close'
		else
			pos = $('.page-container').position()
			left = pos.left
			if left isnt 0
				action = 'close'
			else
				action = 'open'
	switch action
		when 'open'
			$('.disable-dim').css
				'display': 'block'
				'opacity': 0.5
			$('.page-container').css
				'left': -305
			$('#open-menu-btn').hide()
		when 'close'
			$('.page-container').css
				'left': 0
			$('.disable-dim').css
				'display': 'none'
				'opacity': 0
			$('#open-menu-btn').show()
	return

navbar_scroll = () ->
	nav = $('nav')
	if is_mobile()
		nav.removeClass 'navbar-stuck'
	else
		scroll_top = $(window).scrollTop()
		navbar_top = $('header').height() - nav.height()
		if scroll_top >= navbar_top
			if not nav.hasClass 'navbar-stuck'
				nav.addClass 'navbar-stuck'
		else
			nav.removeClass 'navbar-stuck'
	return

navbar_click = () ->
	$('li > a', 'nav').click (e)->
		e.preventDefault()
		$('li', 'nav').removeClass 'active'
		btn = $(this)
		btn.parent().addClass 'active'
		if is_mobile()
			toggle_navbar 'close'
		scroll_to_section btn.attr 'href'
		History.pushState null, btn.text(), btn.attr 'href'
	return

$(window).scroll ->
	navbar_scroll()
	return

$(window).resize ->
	toggle_navbar 'close'
	resize_header_nav()
	navbar_scroll()
	return

retina_img = () ->
	imgs = $('img.retina')
	if window.devicePixelRatio > 1.5
		imgs.each ->
			$(this).attr 'src', $(this).attr('src').replace('.', '@2x.')
	imgs.removeClass 'retina'
	return

init = () ->
	retina_img()
	resize_header_nav()
	$('#open-menu-btn').click (e) ->
		e.preventDefault()
		toggle_navbar()
		return

	$('#close-menu-btn').click (e) ->
		e.preventDefault()
		toggle_navbar('close')
		return

	$('#alumni-trigger').click (e) ->
		e.preventDefault()
		$('#alumni').show()
		$(this).hide()

	History.Adapter.bind window, 'statechange', ->
		State = History.getState()
		return
	current_state = History.getState()
	navbar_scroll()
	navbar_click()
	$('.bxslider').bxSlider
		auto: true
		pause: 5000
		autoControls: true
		useCSS: true
		easing: 'ease-in-out'
		speed: 1000
	$('section, header')
		.waypoint \
			((direction) ->
				link = $(this).attr 'id'
				$('nav li[data-section="'+link+'"]').toggleClass 'active', direction is 'down'
				return
			),
			{
				offset: '100%'
			}
		.waypoint \
			((direction) ->
				link = $(this).attr 'id'
				$('nav li[data-section="'+link+'"]').toggleClass 'active', direction is 'up'
				return
			),
			{
				offset: -> -$(this).height()
			}
	for y in [2021, 2020, 2019, 2018, 2017, 2016, 2015, 2014, 2013, 2012, 2011, 2010, 2009, 2008]
		$('#pub-year-list').append '<a href="#" class="pub-year-btn" rel="'+y.toString()+'">'+y.toString()+'</a>'
	load_year = (year) ->
		$('#pub-list').html 'loading...'
		$.getJSON 'pubs/pubs-'+year.toString()+'.json', (data, textStatus, jqXHR) ->
			$('#pub-list').html ''
			$('.pub-year-btn').removeClass 'pub-year-btn-active'
			for item in data
				pub_item = '<li class="pub-item" id="'+item.EntryKey+'">'
				# authors
				pub_item += '<span class="author">'
				authors = item.Fields.Author
				authors = authors.split(' and ')
				for author in authors
					if authors.length > 1 and author is authors[authors.length-1]
						pub_item += ' and '
					temp_name = author.split(', ')
					if temp_name.length > 1
						temp_name = temp_name[1] + ' ' + temp_name[0]
					else
						temp_name = author
					pub_item += '<span class="author" tag="'+author+'">' + temp_name + '</span>'
					if author is authors[author.length-1]
						pub_item += '. '
					else
						pub_item += ', '
				pub_item += '</span> '
				# title
				pub_item += '<span class="title">'
				pub_item += '<a href="http://dx.doi.org/'+item.Fields.DOI+'">'
				pub_item += item.Fields.Title
				pub_item += '</a>'
				pub_item += '</span>, '
				# journal
				pub_item += '<span class="journal">'
				pub_item += item.Fields.Journal.toLowerCase()
				pub_item += '</span>, '
				# year
				pub_item += '<span class="year">'
				pub_item += item.Fields.Year
				pub_item += '</span>'
				# vol
				pub_item += ' (<span class="vol">'
				pub_item += item.Fields.Volume
				pub_item += '</span>) '
				# page
				pub_item += '<span class="pages">'
				pub_item += item.Fields.Pages
				pub_item += '</span>'

				pub_item += '</li>'
				$('#pub-list').append pub_item
			$('.pub-year-btn[rel='+year.toString()+']').addClass 'pub-year-btn-active'
		return

	$('.pub-year-btn').click (e) ->
		e.preventDefault()
		load_year parseInt($(this).html())
		return
	# load latest pubs
	load_year 2021

	$(window).load ->
		if current_state.hash.substring(1,2) is '#'
			anchor_tag = current_state.hash.substring(2)
			if anchor_tag in ['people', 'research', 'publication', 'contact']
				scroll_to_section anchor_tag
		return
	return

init()

