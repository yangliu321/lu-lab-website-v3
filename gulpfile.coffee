# Load all libraries
gulp       = require 'gulp'
gutil      = require 'gulp-util'
pug        = require 'gulp-pug'
htmlmin    = require 'gulp-htmlmin'
sass       = require 'gulp-sass'
autoprefix = require 'gulp-autoprefixer'
cssmin     = require 'gulp-cssmin'
coffee     = require 'gulp-coffee'
uglify     = require 'gulp-uglify'
header     = require 'gulp-header'
rename     = require 'gulp-rename'
concat     = require 'gulp-concat'
gulpif     = require 'gulp-if'
connect    = require 'gulp-connect'
tempus     = require 'tempusjs'
fs         = require 'fs'
pkg        = require './package.json'

readJSONSync = (json_file) ->
  return JSON.parse fs.readFileSync json_file

build_no = tempus().format '%Y%m%d'
# Create index.html from Pug template
gulp.task 'index', ->
  pug_data =
    members: readJSONSync './src/data/members.json'
    featured_articles: readJSONSync './src/data/featured.json'
    alumni: readJSONSync './src/data/alumni.json'
    build_number: build_no
  banner = fs.readFileSync 'src/header'
  banner += "\
    <!--\n\
        A Website for Prof. Yunfeng Lu's research group at UCLA Chemical \
				Engineering\n\
        Designed and code with love by Yang Liu.\n\
        http://yangliu.name\n\
        Version: #{pkg.version} build #{build_no}\n\
    -->\n\
    "
  gulp.src 'src/index.pug'
    .pipe pug
      data: pug_data
    .pipe htmlmin()
    # .pipe header banner
    .pipe gulp.dest 'build/'
    .pipe connect.reload()

# compress the css
gulp.task 'css', ->
  gulp.src 'src/css/main.css'
    .pipe cssmin()
    .pipe rename 'styles.css'
    .pipe gulp.dest 'build/'
    .pipe connect.reload()

# compress the js
gulp.task 'coffee',  ->
  gulp.src 'src/js/*.coffee'
    .pipe coffee()
    .pipe gulp.dest 'src/js/coffee/'

gulp.task 'js', ['coffee'], ->
  gulp.src ['src/js/plugins.js', 'src/js/coffee/*.js']
    .pipe concat 'main.min.js'
    .pipe uglify()
    .pipe gulp.dest 'build/js/'
    .pipe connect.reload()

gulp.task 'genpubs', ->
  require './genpubs.coffee'
  connect.reload()


gulp.task 'connect', ->
  connect.server
    root: ['build']
    port: 8000
    # livereload: false
    # open:
    # 	browser: "chrome"

gulp.task 'watch-script', ->
  gulp.watch ['src/*.pug','src/data/*.json'], ['index']
  gulp.watch 'src/css/main.css', ['css']
  gulp.watch ['src/js/plugins.js', 'src/js/*.coffee'], ['js']
  gulp.watch 'src/data/pubs.bib', ['genpubs']


gulp.task 'default', ['index', 'css', 'js', 'genpubs']
gulp.task 'watch', ['index', 'css', 'js', 'genpubs', 'connect', 'watch-script'], ->
  gutil.log 'Navigate http://localhost:8000 with your browser.'
  gutil.log 'For docker user under OSX and Windows, you may need to find your IP with the following command'
  gutil.log 'docker-machine ip'
  gutil.log 'Then navigate http://<your ip>:8000 with your browser.'
