#!/bin/bash

if [ -z $HAS_INITIALIZED ]; then
    # set up git
    if [ -z "$MAINTAINER_NAME" ]; then
        echo "Error! Please set MAINTAINER_NAME first."
        exit 1
    fi
    if [ -z "$MAINTAINER_EMAIL" ]; then
        echo "Error! Please set MAINTAINER_EMAIL first."
        exit 1
    fi
    echo "Setting up git user $MAINTAINER_NAME <$MAINTAINER_EMAIL>..."
    git config --global user.name "$MAINTAINER_NAME"
    git config --global user.email "$MAINTAINER_EMAIL"
    git config --global core.editor nano
    git config --global push.default simple


    # set up private key
    [ -z "$MAINTENANCE_KEY_NAME" ] || MAINTENANCE_KEY_NAME="lulabweb-rsa"

    if [ ! -f "/key/$MAINTENANCE_KEY_NAME" ]; then
        echo "Cannot find maintenance key under ./key ."
        exit 1
    fi
    echo "Importing the maintenance key..."
    [ -f /root/.ssh/id_rsa ] && rm -rf /root/.ssh/id_rsa
    [ -d /root/.ssh ] || mkdir -p /root/.ssh
    cp /key/$MAINTENANCE_KEY_NAME /root/.ssh/id_rsa
    chmod 0600 /root/.ssh/id_rsa
fi

function is_git {
    if [ -d .git ]; then
        return 0
    else
       [ -z $(git rev-parse --git-dir 2> /dev/null) ] || return 0
    fi
    return 1
}
function git_status_is_clean {
    if [ -z "$(git status --porcelain)" ]; then 
        # Working directory clean
        return 0
    fi
    return 1
}
function git_ab {
    git rev-list --count --left-right HEAD...origin/master 2>/dev/null
}
function git_is_ahead {
    local aheadbehind
    local ahead
    aheadbehind=$(git_ab)
    IFS=" " read -ra ary <<< "$aheadbehind"
    ahead=${ary[0]}
    if [ $ahead == 0 ]; then
        return 0
    else
        return 1
    fi
}
function git_is_ahead {
    local aheadbehind
    local behind
    aheadbehind=$(git_ab)
    IFS=" " read -ra ary <<< "$aheadbehind"
    behind=${ary[1]}
    if [$behind == 0 ]; then
        return 0
    else
        return 1
    fi
}

ACTION=$1
echo "ACTION=$1"
case "$ACTION" in
    pull)
        cd /repo
        if [ "$(ls -A /repo)" ]; then
            if ! is_git; then
                echo "Your working copy is not a valid git repo. Please consider remove it."
                exit 1
            fi
            if ! git_status_is_clean; then
                echo "CAUTION: Your local copy have uncommitted changes."
                git status
                echo "Make sure to resolve all conflicts before pushing back."
            fi
            echo "Update the local working copy..."
            git pull
            echo "Update NPM modules. This may take a long time."
            npm cache clean
            npm i
            echo "In case there are conflicts, make sure to resolve it first before pushing back."
        else
            echo "Clone the remote code..."
            git clone git@bitbucket.org:yangliu321/lu-lab-website-v3.git /repo
            echo "Update NPM modules. This may take a long time."
            npm cache clean
            npm i
        fi
        exit 0
        ;;
    watch)
        cd /repo
        gulp
        gulp watch
        exit 0
        ;;
    push)
        cd /repo
        if ! is_git; then
            echo "Your working copy is not a valid git repo. Please consider remove it."
            exit 1
        fi
        echo "Committing your changes."
        git add -A
        git commit -a --status -m "$COMMIT_COMMENTS"
        if git_status_is_clean; then
            echo "Push to the origin..."
            git push
        fi
        exit 0
        ;;
    build)
        cd /repo
        gulp
        exit 0
        ;;
    deploy)
        cd /repo
        if ! is_git; then
            echo "Your working copy is not a valid git repo. Please re-initialize your work dir."
            exit 1
        fi
        if ! git_status_is_clean; then
            echo "You have uncommitted changes. Please commit then first before deploying."
            exit 1
        fi
        if git_is_behind; then
            echo "Please pull and merge with the lastest code before deploying."
            exit 1
        fi
        if git_is_ahead; then
            echo "Please push your code to the origin before deploying."
            exit 1
        fi

        echo "Deploying the updates online..."
        /usr/bin/rsync -azP /repo/build/ lu@grad.seas.ucla.edu:/w/fac.2/ch/lu/www
        echo "Please visit http://lu-lab.com with your browser."
        exit 0
        ;;
    develop)
        /bin/bash --rcfile /root/.profile
        exit 0
        ;;
    *)
        echo "Unknown action."
        exit 1
        ;;
esac
