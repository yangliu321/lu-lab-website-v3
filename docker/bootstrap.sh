#!/bin/bash

# update the system
apt-get update

# disable strict host key checking
mkdir -p /root/.ssh
echo "Host *" > /root/.ssh/config
echo "StrictHostKeyChecking no" >> /root/.ssh/config
echo "UserKnownHostsFile /dev/null" >> /root/.ssh/config

# install development tools
apt-get -y install curl nano git rsync
curl -sL https://deb.nodesource.com/setup_4.x | bash -
apt-get -y install nodejs

# install gulp and coffee-script
npm install gulp -g
npm install coffee-script -g


# create working directory
mkdir /repo

# make entrypoint.sh executable
chmod +x /entrypoint.sh

# set some useful aliases
echo "alias pull='/entrypoint.sh pull'" >> /root/.profile
echo "alias push='/entrypoint.sh push'" >> /root/.profile
echo "alias watch='/entrypoint.sh watch'" >> /root/.profile
echo "alias build='/entrypoint.sh build'" >> /root/.profile
echo "alias deploy='/entrypoint.sh deploy'" >> /root/.profile
echo "HAS_INITIALIZED=1" >> /root/.profile


# remove /bootstrap.sh
rm -f /bootstrap.sh