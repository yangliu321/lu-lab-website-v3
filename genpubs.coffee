fs = require "fs"
path = require "path"
parse = require "./bib2json/Parser"
Q = require 'q'
async = require 'async'

class BibTex
  constructor: (@bibtex_file) ->
    @ready = false

  init: () ->
    deferred = Q.defer()
    console.log "Opening file..."
    self = @
    fs.readFile @bibtex_file, 'utf-8', (err, data) ->
      if err
        console.error err
        deferred.reject new Error(err)
      else
        console.log "Parsing BibTex file..."
        self.bib = parse data
        if self.bib.errors.length
          console.error errors
          deferred.reject new Error('no entry in the library')
        else
          console.log "Success!"
          self.ready = true
          self.entries = self.bib.entries
          deferred.resolve 'BibTex file is loaded!'
    return deferred.promise


  # select: ({author, year, journal, key}) ->
  # 	author ?= "*"
  # 	year ?= "*"
  # 	journal ?= "*"
  # 	key ?= "*"

  select_by_year: (year, cb) ->
    year = year.toString()
    year_filter = (item, cb) ->
      if item.Fields.Year is year
        cb true
      else
        cb false

    async.filter @entries, year_filter, (results) ->
      cb results

  years: (cb) ->
    year_list = []
    year_list_func = (item, cb) ->
      if item.Fields.Year not in year_list
        year_list.push item.Fields.Year
      cb null
    async.each @entries, year_list_func, (err) ->
      cb year_list



main = () ->
  json_dir = path.resolve __dirname, 'build', 'pubs'
  if not fs.existsSync json_dir
    fs.mkdirSync json_dir

  pubs = new BibTex path.resolve(__dirname, 'src', 'data', 'pubs.bib')
  pubs_promise = pubs.init()

  pubs_promise.then (()->
    console.log pubs.entries.length + " publications are loaded from BibTex library."
    pubs.years (years) ->
      async.each years, ((item, cb) ->
        pubs.select_by_year parseInt(item), (entries) ->
          console.log "Year #{item}, #{entries.length} publications."
          json_file = path.resolve json_dir, "pubs-#{item}.json"
          console.log "Exporting Year #{item} publications (#{entries.length}) to JSON file (#{json_file})..."
          fs.writeFile json_file, JSON.stringify(entries), (err) ->
            if err
              console.error "Export Year #{item} publications failed! (#{err})"
            else
              console.log "Export Year #{item} publications successfully!"

        cb null
      ), (err) ->
        console.log "DONE"

  ), (error) ->
    console.error error
    process.exit 1

main()
