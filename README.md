# LuLab Website V3
===

## What's LuLab Website?

LuLab website is a website for Prof. Yunfeng Lu's group at UCLA Chemical Engineering. The main purpose of this website is to introduce the researches in Lu group to the world. It also serves as a portal for people to wanna get in touch with Prof. Yunfeng Lu, and the members in Lu group.

## Who made this website?

LuLab website v3 ~~is~~ is used to be a one-man-project made by [Yang Liu](http://yangliu.name).

*Update:* Xing Lu & Duo Xu start maintaining the website from June, 2016.

## I wanna update some information on the website.

Currently, ~~only members in LuLab can request the modification to their own information. If you need to change the info, contact Yang Liu via email (me@yangliu.name).~~

Please contact Duo Xu or Xing Lu via the aforementioned contact info for updating your information.

# Guide For Maintainers

## Background information

(omitted 10000 words...)

__tl;dr; version__

* For beginners, please use docker containers for maintenance.
* For advanced maintainers, please read the code.
* Recommended editors: [Visual Studio Code](https://code.visualstudio.com/), [Sublime Text](https://www.sublimetext.com/), [Atom](https://atom.io/), or [Vim](http://www.vim.org/).

## For Beginners - Using Docker

Operating system: OSX Moutain Lion/Yosemite/El Captain, Windows 8/8.1/10, or Linux.

### Setup Development Environment

1. **Install docker** 

    Please follow the instructions on [docker's website](https://www.docker.com).

    Basically, you need to install Docker Engine on Linux, or you need to install (Docker Toolbox)[https://www.docker.com/products/docker-toolbox] if you use OSX or Windows.

2. **Create your working directory/folder**

    Create the working directory with your file manager/Finder(OSX)/File Explore(Win)/cli for storing all the code and maintenance scripts. For OSX and Windows, make sure the working folder is under your home folder. For example, the working folder could be

    _OSX/Linux:_ `~/lulab-web`

    _Windows:_ `C:\Users\<yourname>\lulab-web`

    The following guide will use the above directory for demostration.

3. **Install the maintenance scripts**

    * open __docker-cli__(OSX/Linux) or __Docker Quickstart Terminal__(Windows). It may take a long time to start for the first time. Please be patient. For Windows user, you may need to click "Yes" for multiple times due to UAC.
    * enter your working directory with the following command

    `cd lulab-web`

    * install the scripts with following command

    `curl -L https://bitbucket.org/yangliu321/lu-lab-website-v3/downloads/install.sh | sh`

    * initialize your working directory

    `./luweb init`

    You will be then asked for your name and your email address. Please enter it accordingly and they will be used to log and track for website updates.

4. **Obtain and Install the maintenance key**

    * Ask Yang or any maintainer for the maintenance key

    * please copy your maintenance key file into _lulab-web/key_, make sure the filename is __lulabweb-rsa__.
   
    __Please make sure to keep this key file in a safe place. Anyone with this key can do whatever naughty things to Lu Lab's website.__

5. **Basic workflow**

    After setting up the development environment, you should have all necessary tools installed in your system. Please do follow the workflow described below when you do any modifications to the website.

* __STEP 0.__ open __docker-cli__(OSX/Linux) or __Docker Quickstart Terminal__(Windows), and enter your working directory

    `cd lulab-web`
    
* __STEP 1.__ Pull the code to your local working directory

    `./luweb pull`

    This command will sync your local working dir with the remote one. It will also install all necessary modules automatically for the development (may take really long time). You should run this command before you making changes to the website. Usually, the script will notify you to run this 'pull' command if your local copy is out of sync.

* __STEP 2.__ Open your favorite editor, and make your modifications

    For daily maintenances, only files under the following directories need to be changed.

    1. __repo/src/data/members.json__: Data of current lab members. JSON format.
    2. __repo/src/data/alumni.json__: Data of alumni. JSON format.
    3. __repo/src/data/pubs.bib__: All our publications. Bibtex format.
    4. __repo/build/img/avatars__: All profile pictures of lab members are stored here. Usually, two resolution of the profile picture should be provided - a low resolution one (210px * 210px) named with __mem-_name_.jpg__, and a high resolution one (420px * 420px) named with __mem-_name_@2x.jpg__.

    Other directories

    * __repo/__: Main directory, synced with the remote repo.
    * __repo/.git__: Version tracking data, should never be touched.
    * __repo/bib2json__: Bibtex to JSON converting program.
    * __repo/build__: Files that are need to be uploaded to the server. However, you do not need to upload it manually in most cases.
    * __repo/docker__: Files and scripts that are needed to build docker image.
    * __repo/node_modules__: Modules that are needed for Gulp.js. Should never be touched.
    * __repo/SDK__: Some useful design templates in case you would like to update the look and feel of the website.
    * __repo/src__: The source code the the webside, including the page template (*.mug), stylesheets (css/*), javascripts (js/*), and data used for generating the website.

* __STEP 3.__ After making modifications, you should test your code.

    _For OSX & Windows:_ run `docker-machine ip` and write the ip address, then

    `./luweb watch`

    Wait until the script showing _"Listen at http://127.0.0.1:8000"_, then open your browser, and type

    `http://<ip address>:8000`

    Examine the website. In case you find anything wrong, you can make changes to the code immediately with your editor. As soon as you save your changes, the script will build the website automatically, and all you need to do is press the refresh button of your browser.

    If everything looks good, go back to the CLI, and press __CTRL+C__ to end testing.

* __STEP 4.__ Push your changes to the remote repo.

    You are required to do this step before you upload the updates. This will be extremely helpful for tracking the changes and restore the website to its working status in case something goes wrong. To do it, enter the command at CLI

    `./luweb push`

    You will be notified to describe your changes. Please enter a few words to describe your changes (e.g., add new member XXX), otherwise you will not able to update the website.

* __STEP 5.__ Build the website.

    `./luweb build`

    This will generate the working HTML/CSS/JS code and do all necessary optimization.

* __STEP 6.__ Deploy to the server.

    `./luweb deploy`

    The script will upload the website automatically. Please note if you have untracked changes (means you didn't do Step 4), the script will refuse to deploy. Sometimes your local copy may be out of sync with the remote one (usually caused by working on multiple computers), you may need to do Step 1, then Step 4 before deploying.

    After deploy, you can check the updated website at http://lu-lab.com

* __STEP -.__ You do not need to remove your working directory. Actually, keeping the working directory will significantly reduce the time for pulling and deploying the code.      


## For Advanced Maintainers - Setup a native developing environment

**Disclaimer:** The following guide is for advanced user only. If you don't feel comfortable with any of the following words (git, node.js, javascript/coffeescript, html/css, basic *nix cli operations), please go back with the beginner's guide. 

### Some information

* This website uses [Gulp](http://gulpjs.com) as automating build system, and Gulp.js needs Node.js.
* Daily maintenance should only need to touch files under **./src** and **./build** unless you know exactly what you're doing.
* **./build** is the only directory needed to be uploaded.
* All updates to the code and data should be tracked and pushed back to this repo with git.

### Server info

We use the web space provided by SEASnet to host our website. Here's the information you may need.

* _Host:_ `grad.seas.ucla.edu`
* _Port:_ `22`
* _Protocol:_ `SFTP/SCP/SSH`
* _User:_ `lu`
* _Password:_ __Not Applicable__, use the maintenance key instead.

Please ask Yang or any maintainer for the maintenance key and install it to your system.

### Setup Development Environment and Update the website

1. Install git, node.js, and npm.
2. Clone this repo.

    `git clone https://yangliu321@bitbucket.org/yangliu321/lu-lab-website-v3.git`

3. Modify the files as you need.
4. Update/install the build dependences

    In your working directory, `npm i`

5. Test.

    `gulp watch`

6. Build.

    `gulp`

7. Push your changes back to the repo with git

    `git add -A`

    `git commit`

    `git push`

8. Upload build/* to the server (find the server info above)

## How to generate publication list

The easiest way to generate an up-to-date publication list is using Web of Science.

1. Visit Web of Science using UCLA Campus network or VPN. [LINK](http://uclibs.org/PID/12610)
2. Click the drop-down arrow along with **Basic Search**, choose **Author Search**.
3. Fill *Last Name* with **LU**, and *Initial* with *Yunf*, click **Select Research Domain** button.
4. Choose **LIFE SCIENCES ...**, **PHYSICAL SCIENCES**, and **TECHNOLOGY**, click **Select Organization** button.
5. Find **University of California Los Angeles**, check it and then click **Finish Search**
6. Scroll to the end of the page, choose **Show 50 per page**.
7. Check **Select Page**, then click the drop-down button along with the *save to*, click **Save to other file format**.
8. Select "All records on page", "Author, Title, Source, Abstract", choose **BibTeX** as File Format, then click **send**.
9. Repeat 7-9 for the rest pages.
10. Now you should have several **.bib** files, let's say they are *1.bib*, *2.bib*, *3.bib*.
11. Concatenate them together in order, you can do it with any text editor (but not Word/Pages/WPS for the god sake) and save the new file with name "pubs.bib". If you are using macOS or Linux, you can simply achieve it with the following command

    `cat 1.bib 2.bib 3.bib > pubs.bib`

12. Place `pubs.bib` to `<your working dir>/repo/src/data`, you may want to back up the original `pubs.bib` in case there is anything wrong.


## Old Readme

## What's new (already old)?

### One-page Design

The new design (v3) combines all the necessary info inside one single page. One-page design makes users easier to get the information in the website, without multiple clicks for navigation.

### Flat & minimal design, Large & sharp fonts

We have adopt the several criteria of the modern web design - flat & minimal design, and large and sharp font. The font we choose for the new design is Raleway from Google Web Font. Most pictures have been reproduced with vector format (SVG), so you will see very sharp and clear pictures no matter what screen resolution you are using.

### Retina support

In the year 2011, Apple introduced the retina display to the world with the release of iPhone 4. From that time, designers will never be restricted by the 72 DPI resolution. Nowadays, a lot of computers (Macbook Pro with Retina Display) and mobile devices (iPhone 4/4S/5, The new iPad, Android phones and tablets, Sony PS Vita, Sony 4K TV, etc) equip a screen with pixel density higher than 200 DPI. We named those screens as retina display. In our new design, the website will detect your device automatically, and serve high-resolution pictures if you're using a device with retina display. That will release the full power of the retina display of your device.

Note, some of the avatars are not retinafied due to the lack of resources.

### One design, all devices

LuLab Website v3 utilized the CSS3 media queries to achieve a responsive design for all your devices. Now, you can browse LuLab website not only on your computer, you can also browse it with your smartphones, tablets, TVs, and even game consoles. As the initial release of the new design, LuLab Website v3 has been tested with

* Internel Explore 9/10/11 on Windows 7/8
* Safari 6/7/8 on Mac OS X
* Mozilla Firefox 20 on Windows, Mac OS X, Ubuntu and Arch Linux
* Google Chrome/Chromium on Windows, Mac OS X, and Arch Linux
* iPhone 4/4S/5/6/6s with Mobile Safari on iOS 6/7/8/9
* Chrome on Android 4.1/4.4.2/Kitkat/M/N with Sumsung Galaxy S, Google Nexus 5 and Nexus 7, Nexus 6P
* Safari and Chrome on iPad 1 (iOS 5), The new iPad (iOS 6/7/9), and iPad Pro 12.9 (iOS 9)
* Minix Neo X7 (Android 4.2.2, Chrome and Dophin) with 40' LCD TV
* Modori on Rasbian (Raspberry Pi B, 2, 3, Zero)
* Sony PS Vita

I have extensively tested the new design with all devices I can access, including all main-stream browsers, smartphones, tablets, embeded systems, TVs, and even game consoles. If you meet any problems in browsing the website, please upgrade your browser, or switch to a A-Grade browser (IE9, Firefox 4+, Safari 5+, Google Chrome). So try it today with your smartphone and tablet!

## What's new in v3.1

In v3.1, gulf.js task runner system is introduced to simply the update and publishing process.

## How to contact the designer?

Please contact Yang Liu via email (me@yangliu.name), or by visiting his website (http://yangliu.name).

You can also contact the maintainer Xing Lu and Duo Xu via their emails (find it at http://lu-lab.com).

